#!/bin/sh

cd lastdooby_backup

rm -r Assets
rm -r DOC
rm -r GD
rm -r LD
rm -r SCH
rm -r ST

cd ..
cd ..
cd GD\&P

cp -r Assets /mnt/c/Repos/GD\&P_Backup/lastdooby_backup
cp -r DOC /mnt/c/Repos/GD\&P_Backup/lastdooby_backup
cp -r GD /mnt/c/Repos/GD\&P_Backup/lastdooby_backup
cp -r LD /mnt/c/Repos/GD\&P_Backup/lastdooby_backup
cp -r SCH /mnt/c/Repos/GD\&P_Backup/lastdooby_backup
cp -r ST /mnt/c/Repos/GD\&P_Backup/lastdooby_backup

echo "Finito capo!"
