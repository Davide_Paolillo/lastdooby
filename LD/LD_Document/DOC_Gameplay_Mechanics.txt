L'esploarazione si sviluppera' in due modalita':
- Free roaming
- Combattimento a turni

Free Roaming:

La parte di free roaming e' prevista per le varie fasi di esplorazione delle varie mappe in cui si articola il gioco.
La visuale sara' in terza persona dell'alto, con tre telecamere, una per ogni personaggio controllabile dal giocatore (Minerva, Pomona, Filius),
al giocatore e' consentito cambiare personaggio da controllare ogni qualvolta lo desideri, questo sara' utile se vorra' utilizzare
alcune abilita' specifiche possedute da una particolare spec di un personaggio (ovviamente attivabili in modalita' free roaming).
Ad ogni personaggio potra' essere assegnata una diversa spec, tra le tre possibili; ovviamente alla fine ognuno di loro possiedera' una diversa
spec.
Ogni personaggio possiede da subito delle abilita' base, che consentono di superare le prime fasi di gioco; inoltre queste spell saranno
utilizzabili in modalita' free roaming per superare enigmi ambientali, o orientarsi all'interno del mondo di gioco stesso.

Combattimento a turni:
Vedi DOC Combat System

RUOLISTICA

Magie spec generica:

Accio - attiro piccoli oggetti
Depulso - Sparo via oggetti
Wingardium Leviosa - incantesimo di levitazione; fa levitare gli oggetti a mezz’aria


Spec:

Anathema:
Aguamenti - Produce getto di acqua
Aqua Eructo - Produce un enorme e violento getto d’acqua
Artis Tempurus - Genera un vortice di fuoco
Avada Kedavra - Instakill, aumenta corruzione, richiede molte risorse
Bombarda - Fa esplodere oggetto vicino a nemico, se disponibile oggetto -> si evolve in Bombarda Maxima
Excelsiosempra - incantesimo di dispersione; scaglia in aria l’avversario per poi farlo ricadere giù violentemente
Fastronum - crea un suono molto forte che disorienta l’avversario
Silencio - incantesimo tacitante; fa sparire la voce a persone e ad animali (Silence)
Stifling - fattura che causa il soffocamento dell’avversario

Fascinatio:
Incendio - genera fiamme dalla punta della bacchetta, applica DoT
Lacarnum Inflamare - appicca un incendio, applica DoT
Alimentes Flames - Alimenta fuoco, facendolo diventare enorme, amplifica DoT da fuoco, possibile tirare solo se esiste gia' un fuoco
Ardemonio - Spawna un aiutante che attacca il bersaglio con magie di fuoco (spawna un aiutante random scelto tra drago chimera o serpente)
Avis - Evoca uno stormo di uccelli che attacca o acceca il nemico
Crucio - maledizione della tortura, aumenta corruzione, è una delle tre Maledizioni Senza Perdono; tortura l’avversario, che si sente come perforato da mille coltelli bollenti
Ebublio - fattura che abbatte l’avversario con bolle esplosive
Sectumsempra - maledizione che taglia e lacera varie parti del corpo

Adiumentum:
Anapneo - Libera vie respiratorie, evita soffocamento (dispell per silence)
Animaleus Nobodyx - Fa ritornare ipotetici NPC animagus nemici allo status di umani, quindi amici
Aresto Momentum - riduce o annulla del tutto il movimento di un corpo (fa saltare il turno ad un nemico)
Ascendo - Fa compiere un balzo enorme a chi pronuncia l’incantesimo (aumento potenza abilita' per i prossimi turni TBD)
Confundo (o Confundus) - confonde un avversario per un breve periodo di tempo, nel quale fa cose prive di senso
Dismundo - fa avere visioni spaventose all’avversario per distrarlo
Emendo - guarisce una parte del corpo, cura potente
Expecto Patronum - incanto Patronus; crea un Patronus, che difende il mago dai Dissennatori, produce una forte luce bianca e assume la forma di un animale, diverso per ogni persona
Ferula - fa apparire delle bende che si avvolgono attorno ad una ferita
Fianto Duri - incantesimo di protezione; respinge gli incantesimi avversari
Incantesimo Freddafiamma - fa diventare freddo il fuoco
Imperio - maledizione del comando, aumenta corruzione, è una delle tre Maledizioni Senza Perdono; permette di comandare a bacchetta, senza che esso lo voglia, un uomo o un animale

Per livellare le varie magie il giocatore sblocchera' Z punti abilita' per ogni livello, e sara' libero di decidere come spenderli nell'albero abilita' situato all'interno del pensatoio.
Gli Z punti saranno < rispetto ai D punti totali necessari a sbloccare tutte le magie, in questo modo quali e quante magie sbloccate dipendera' dal giocatore stesso che dovra' spendere
i suoi Z punti in maniera oculata.

CORRUZIONE

Ogni spec possiede una sola Maledizione Senza Perdono, queste sono potentissime e hanno effetti devastanti,
permettono di cambiare un combattimento gia' perso, rendendolo vincente, al costo dell'aumento di una barra di corruzione,
all'aumentare di questa barra il tempo di ricarica ed il costo di risorse delle maledizioni senza perdono diminusce,
piu' la barra aumenta piu' Minerva si avvicina al lato oscuro, se la barra si riempie totalmente e' game over e Minerva si unira' ai mangiamorte,
se la barra non viene purificata spesso a fine game si avra' un bad ending.
Per purificare la barra Minerva dovra' compiere scelte buone (ad intuito, il gioco non indichera' nulla) disponibili solo in poche fasi del game,
dovendo cosi' getire la barra in maniera maniacale ed attenta (questo dipende dalla modalita' di gioco scelta, a modalita' facile magri si semplifica tutto).

MECCANICHE IN GAME

Per scegliere la spec ed imparare nuove mosse Minerva si dovra' recare nel Pensatoio all'interno dell'ufficio di Silente, nel quale trovera' di volta
in volta ricordi che se scelti fanno imparare una spec o una determinata mossa; una volta imparate tutte le possibili spell queste aumenteranno di potenza in base
alla corruzione di Minerva, piu' e' alta la corruzione piu' le spell diventano potenti e meno costose, alcune si possono evolvere. (un po' di corruzione e' sempre necessaria)
Minerva puo' scegliere una spec, stessa cosa per Filus e Pomona, ma ognuno dovra' averne una diversa.

EXPERIENCE GAIN
Ogni nemico fornisce un quantitativo di EXP standard, Minerva e compagni guadagneranno questa EXP in percentuale in base al livello del 
nemico affrontato, ovvero:
- Nemico stesso livello 100% EXP
- Nemico piu' basso di un livello 50% EXP
- Nemico piu' basso di due livelli 0% EXP
- Nemico piu' alto di un livello 130% EXP
- Nemico piu' alto di due livelli 170% EXP
Le statistiche di un nemico variano in base a quanti livelli e' superiore/inferiore a Minerva e compagni, nemici di livello tre volte
o piu' superiore a Minerva e compagni non potranno essere affrontati in quanto molto piu' forti della compagnia.
Per avanzare e sconfiggerli i ragazzi dovranno prima accumulare exp per salire di livello per poi affrontarli.